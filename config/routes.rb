Rails.application.routes.draw do
  get 'contato/index'
  get 'sobre/index'
  get 'galeria/index', to: "galeria#index"
  get 'galeria/index/:id', to: "galeria#index"
  get 'reserva/index'
  get 'menu/index'
  root to: "home#index"
  get 'home/index'
end
