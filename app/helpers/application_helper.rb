module ApplicationHelper
	def self.url_base(args)
		url_development = 'https://cmppostgres.herokuapp.com/api/v1/buffalo/'+args
		url_production = 'https://cmppostgres.herokuapp.com/api/v1/buffalo/'+args
		result = url_development if Rails.env == 'development'
		result = url_production if Rails.env == 'production'
		result
	end
end
